Components
==========

Components are reusable pieces of functionality that can be invoked via a controller or react to controller events. This is a great way to re-use code between controllers, such as with pagination, searching, emailing, etc.


Using Components
----------------

Import the components and include them in the ``Meta.components`` property on your controller, like so::

    from ferris.components.pagination import Pagination
    from ferris.components.search import Search

    class Documents(Controller):
        class Meta:
            components = (Pagination, Search)

Inside of your actions, you can access the component instances using ``self.components``::

    def list(self):
        self.components.pagination()


Creating Components
-------------------

You can create your own components. Here's a very simple component that listens for the startup event and logs::

    class Logger(object):
        def __init__(self, controller):
            self.controller = controller
            self.controller.events.after_start += self.on_after_startup

        def on_after_startup(self, controller):
            logging.info("Hello!")



Built-in Components
-------------------

.. module:: ferris.components

The :mod:`ferris.components` module provides a few built-in components. These can be used directly, customized, or used as guidelines when building your own.


EdgeCache
~~~~~~~~~

.. module:: ferris.components.edge_cache

The Edge Cache component provides utilities for doing client-side caching. This is not to be confused with memcache which is server-side caching.

.. autoclass:: EdgeCache

Usage is pretty easy. Include the edge cache component and use the ``set`` decorator.

.. autofunction:: set

For example::

    from ferris import Controller, route
    from ferris.components import edge_cache

    class Posts(Controller):
        class Meta:
            components = (EdgeCache,)

        @route
        @edge_cache.set('public', 60*60)
        def list_all(self):
            ...

        @route
        @edge_cache.set('private', 60*60)
        def list_mine(self):
            ...

OAuth
~~~~~

The OAuth component is documented in :doc:`oauth2`.


Pagination
~~~~~~~~~~

.. module:: ferris.components.pagination

.. autoclass:: ferris.components.pagination.Pagination

.. automethod:: ferris.components.pagination.Pagination.paginate

For example of using this, see :doc:`../tutorial/7_extras`

You can also use the pagination component to provide pagination for custom datasets such as external apis or CloudSQL. The pagination component can automatically keep track of previous cursors to provide backwards pagination. For example::

    cursor, limit = self.components.pagination.get_pagination_params()

    data, next_cursor = get_custom_data(cursor, limit)

    self.components.pagination.set_pagination_info(
        current_cursor=cursor,
        next_cursor=next_cursor,
        limit=limit,
        count=len(data)
    )

.. automethod:: ferris.components.pagination.Pagination.get_pagination_params

.. automethod:: ferris.components.pagination.Pagination.set_pagination_info

.. automethod:: ferris.components.pagination.Pagination.get_pagination_info


.. _pagination_macros:

There is also a helpful macro to print simple forward and backwards pagination controls::

    {% import "macros/pagination.html" as p %}

    {{p.next_page_link()}}


.. method:: PaginationMacros.next_page_link()

    Generates bootstrap style forward and backwards pagination arrows.


Upload
~~~~~~

The upload component is documented in the :doc:`uploads_and_downloads` section.

Search
~~~~~~

The search component is documented in the :doc:`search`.


Cross-Site Request Forgery Protection
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. module:: ferris.components.csrf

.. autoclass:: ferris.components.csrf.CSRF

.. autofunction:: ferris.components.csrf.csrf_protect
