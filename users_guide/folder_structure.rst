Folder Structure
================

Root Directory Structure
---------------------------

The root of your application contains the following items:

 * **app** - Contains code related specifically to your application. See below for a detailed description of each sub-folder.
 * **ferris** - Contains Ferris' core code. Typically you won't need to modify anything in here unless you're updating Ferris.
 * **plugins** - Contains re-usable components that you can use from your application. See :doc:`plugins` for more information.
 * **packages** - Contains 3rd party libraries (such as pytz, google apis, etc.). See :doc:`packages` for more information.
 * **app.yaml** - Normal App Engine configuration.
 * **settings.py** - Global application (and plugin) configuration. You can load these settings by using the :doc:`settings` API.


Application Directory Structure
-------------------------------

The app folder contains the following items:

 * **models** - Contains :doc:`model <models>` modules.
 * **controllers** - Contains :doc:`controller <controllers>` modules.
 * **templates** - Contains template files and macros used by :doc:`views`.
 * **static** - Contains :doc:`static resources<static_files>` such as javascript, css, and images.
 * **tests** - Contains any :doc:`test specs <testing>`.
 * **routes.py** - Contains :doc:`route <routing>` and :doc:`plugin <plugins>` configuration. You can enable plugins and change routing rules here. As most routing happens automatically, you'll use this primarily to enable plugins.
 * **listeners.py** - Contains event listeners for the :doc:`global event bus <events>`. You can use this to set the applications theme and enforce application-wide security settings.
