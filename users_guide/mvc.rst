How Ferris does MVC
===================

Ferris is similar to a lot of other MVC frameworks such as Rails or CakePHP, though the terminology may be slightly different:

* :doc:`models` handle the retrieval and storage of data in the App Engine Datastore. Models are simply Google App Engine `ndb.Model <https://developers.google.com/appengine/docs/python/ndb/>`_ subclasses with a few helper methods. Typically a most of the business logic is implemented in the model layer. This encourages code-reuse and make it easier to test all of your business logic.
* :doc:`controllers` are responsible for responding to HTTP requests. Controllers are typically thin glue between models and views. Controllers have special methods called *actions* that are accessable via HTTP.
* :doc:`views` handle the presentation of data to the user. Views have access to data that is retrieved by the controller. Views can render HTML, JSON, or other representations.
* :doc:`routing` maps urls to controller actions. This happens automatically, but Ferris allows custom routes and redirects.
* :doc:`scaffolding` can automatically create CRUD (create, read, update, delete) actions and templates for you and can be customized and augmeneted to fit your needs.
* :doc:`templates` can be rendered by the views. Ferris uses the `jinja2 <http://jinja.pocoo.org/>`_ template engine.
* :doc:`forms` and :doc:`messages` are ways of representing data submitted by a client that's used to update a model. Forms are better suited for HTML pages, whereas messages are better suited for JSON consumers. These are exposed to the controller via :doc:`request parsing <request_parsers>`.
* :doc:`components` are a way of packaging common controller functionality into reusable classes.
* :doc:`behaviors` are a way of packaging common model functionality into reusable classes.
* :doc:`plugins` are a way of packaging entire parts of an application to be reused in another. If you're coming from Django, plugins should seem very similar to the app concept in that framework. 
